/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tap34pracfx;

import javafx.fxml.Initializable;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
/**
 * FXML Controller class
 *
 * @author Ivan
 */
public class ImitadorController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private RadioButton RB1;

    @FXML
    private ToggleGroup Group1;

    @FXML
    private RadioButton RB2;

    @FXML
    private RadioButton RB3;

    @FXML
    private CheckBox op1;

    @FXML
    private CheckBox op2;

    @FXML
    private CheckBox op3;

    @FXML
    private TextField tf;

    @FXML
    private ComboBox<?> spp;

    @FXML
    private Spinner<?> sp;

    @FXML
    private RadioButton rb4;

    @FXML
    private RadioButton rb5;

    @FXML
    private RadioButton rb6;

    @FXML
    private CheckBox op4;

    @FXML
    private CheckBox op5;

    @FXML
    private CheckBox op6;

    @FXML
    private TextField tf2;

    @FXML
    private ComboBox<?> spp1;

    @FXML
    private Spinner<?> sp1;

    @FXML
    void accionbt1(ActionEvent event) {

    }

    @FXML
    void accionbt2(ActionEvent event) {

    }

    @FXML
    void accionbt3(ActionEvent event) {

    }

    @FXML
    void accionop1(ActionEvent event) {

    }

    @FXML
    void accionop2(ActionEvent event) {

    }

    @FXML
    void accionop3(ActionEvent event) {

    }

    @FXML
    void cbaccion(ActionEvent event) {

    }

    @FXML
    void textaccion(KeyEvent event) {

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
